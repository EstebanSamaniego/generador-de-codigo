import os 
import shutil
import siebstr
import siebconf

class Geco():
	"""docstring for Geco"""
	def __init__(self, modelo):
		super(Geco, self).__init__()

		self.modelo 				= modelo
		self.file_resource_name 	= '{0}Resource.java'.format(self.modelo)
		self.file_bussiness_name	= '{0}BS.java'.format(self.modelo)


	def escribe_archivo_bs(self):
		f = open(self.file_bussiness_name, 'w')

		f.write((siebstr.bs_imports 		+
				 siebstr.bs_class_signature +
				 siebstr.bs_constructor 	+ 
				 siebstr.bs_find_all 		+ 
				 siebstr.bs_find_by_id 		+
				 siebstr.bs_save			+
				 siebstr.bs_update)
		 .format(self.modelo, 
		 		 self.modelo.lower(),
		 		 siebconf.ear, 
		 		 siebconf.ejb,
		 		 siebconf.facade,
		 		 siebconf.bussiness,
		 		 'jpa'))

		f.close()

		#self.move_file(self.file_bussiness_name, self.ruta_bussiness)

	def escribe_archivo_resource(self):			
		f = open(self.file_resource_name, 'w')

		f.write((siebstr.jersey_imports 		+ 
				 siebstr.jersey_class_signature	+ 
				 siebstr.jersey_find_all 		+ 
				 siebstr.jersey_find_by_id 		+ 
				 siebstr.jersey_post 			+ 
				 siebstr.jersey_put)
		 .format(self.modelo, 
		 		 self.modelo.lower(), 
		 		 siebconf.bussiness))

		f.close()

		#self.move_file(self.file_resource_name, self.ruta_resources)
		
	def move_file(self, origen, destino):
		shutil.move(os.path.join(os.getcwd(), origen), destino)

def main():
	m = input('Cual es el modelo: ')

	geco = Geco(m);
	geco.escribe_archivo_bs()
	geco.escribe_archivo_resource()

if __name__ == '__main__':
	main()
