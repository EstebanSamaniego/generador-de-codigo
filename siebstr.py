#Strings para la clase Bussiness
bs_imports = ('package {5};\n\n' +
			  'import java.util.List;\n\n' +
			  'import javax.naming.Context;\n\n' +
			  'import {4}.{0}FacadeRemote;\n' +
			  'import {6}.{0};\n\n')

bs_class_signature = ('public class {0}BS extends EjbConnection {{\n\n' +
					  '\tprivate {0}FacadeRemote {1};\n\n')

bs_constructor = ('\tpublic {0}BS(){{\n' +
				  '\t\ttry{{\n' +
				  '\t\t\tContext context = createRemoteEjbContext();\n' +
				  '\t\t\tString url = \"ejb:{2}/{3}/{0}Facade!{4}.{1}FacadeRemote\";\n' +
				  '\t\t\tthis.{1} = ({0}FacadeRemote) (({0}FacadeRemote) createEjbProxy(context, url, {0}FacadeRemote.class));\n' +
				  '\t\t}}catch(Exception e){{\n' +
				  '\t\t\te.printStackTrace();\n' +
				  '\t\t}}\n' + 
				  '\t}}\n\n')

bs_find_all = ('\tpublic List<{0}> findAll(){{\n' +
					  '\t\treturn this.{1}.findAll();\n' +
					  '\t}}\n\n')

bs_find_by_id = ('\tpublic {0} get{0}ById(short id){{\n' +
				 '\t\treturn this.{1}.findById(id);\n' +
				 '\t}}\n\n')

bs_save = (	'\tpublic {0} save{0}({0} entity){{\n' +
			'\t\treturn this.{1}.save(entity);\n' +
			'\t}}\n\n')

bs_update = ('\tpublic {0} update{0}({0} entity){{\n' +
			 '\t\treturn this.{1}.update(entity);\n'
			 '\t}}\n' +
			'}}')

#Strings para la clase jersey
jersey_imports = ('package {2};\n\n'
					'import java.sql.Timestamp;\n' +
					'import java.util.List;\n\n' +
					'import javax.ws.rs.Consumes;\n' +
					'import javax.ws.rs.GET;\n' +
					'import javax.ws.rs.POST;\n' +
					'import javax.ws.rs.PUT;\n' +
					'import javax.ws.rs.Path;\n' +
					'import javax.ws.rs.PathParam;\n' +
					'import javax.ws.rs.Produces;\n' +
					'import javax.ws.rs.core.MediaType;\n' +
					'import javax.ws.rs.core.Response;\n\n' +
					'import {2}.{0}BS;\n\n')

jersey_class_signature = ('@Path(\"{1}\")\n' +
					'public class {0}Resource {{\n' +
					'\n');

jersey_find_all = ('\t@GET\n' +
				'\t@Produces(MediaType.APPLICATION_JSON)\n' +
				'\tpublic List<{0}> get{0}(){{\n' + 
				'\t\t{0}BS {1} = new {0}BS();\n' +
				'\t\treturn {1}.findAll();\n' +
				'\t}}\n\n')

jersey_find_by_id = ('\t@GET\n' +
				'\t@Path(\"{0}/{{id}}\")\n' +
				'\t@Produces(MediaType.APPLICATION_JSON)\n' +
				'\tpublic {0} get{0}ById(@PathParam("id") int id){{\n' +
				'\t\t{0}BS {1} = new {0}BS();\n' +
				'\t\treturn {1}.get{0}ById(id);\n' +
				'\t}}\n\n')


jersey_post = 	('\t@POST\n' +
			  	'\t@Path(\"{1}\")\n' +
				'\t@Consumes(MediaType.APPLICATION_JSON)\n' +
				'\t@Produces(MediaType.APPLICATION_JSON)\n' +
				'\tpublic {0} insert{0}(Ofertas entity){{\n' +
				'\t\t{0}BS {1} = new {0}BS();\n' +
				'\t\tentity.setFechaCreacion(new Timestamp(System.currentTimeMillis()));\n' +
				'\t\treturn {1}.save{0}(entity);\n' + 
				'\t}}\n\n')

jersey_put = ('\t@PUT\n' + 
			'\t@Path("{1}")\n' + 
			'\t@Produces(MediaType.APPLICATION_JSON)\n' + 
			'\t@Consumes(MediaType.APPLICATION_JSON)\n' + 
			'\tpublic {0} updatePersona({0} entity){{\n' + 
				'\t\t{0}BS {1} = new {0}BS();\n' + 
				'\t\treturn {1}.update{0}(entity);\n' + 
			'\t}}\n\n')